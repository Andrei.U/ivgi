import unittest
from app.objects import Stream
from app.objects import AssetPoint
from app.objects import Asset
import time


class StreamTestCase(unittest.TestCase):
    pass


class AssetTestCase(unittest.TestCase):
    data_once = [{"ask": 1000, "bid": 900}]
    data_set = [{"ask": 150, "bid": 160},
                {"ask": 123, "bid": 125},
                {"ask": 120, "bid": 130},
                {"ask": 100, "bid": 110}]
    asset_name = 'test_asset'

    @staticmethod
    def printer(name: str, asset: Asset):
        print('{}\nMax:{}, Min:{}, Avr:{}, Diff:{}, Med:{}'.format(name, asset.max, asset.min, asset.avr,
                                                                   asset.points[0].diff, asset.points[0].med))

    def test_set_method(self):
        asset = Asset(self.asset_name)
        asset.add(self.data_once[0], str(round(time.time(), 0)))
        AssetTestCase.printer(AssetTestCase.test_set_method.__name__, asset)
        self.assertEqual(len(asset.points), 1)

    def test_many_set(self):
        asset = Asset(self.asset_name)
        for point in self.data_set:
            asset.add(point, str(round(time.time(), 0)))
        AssetTestCase.printer(AssetTestCase.test_many_set.__name__, asset)
        self.assertEqual(len(asset.points), len(self.data_set))

    def test_min(self):
        asset = Asset(self.asset_name)
        for point in self.data_set:
            asset.add(point, str(round(time.time(), 0)))
        temp = []
        for point in self.data_set:
            temp.append(AssetPoint.get_med(point['ask'], point['bid']))
        self.assertEqual(asset.min, min(temp))


class AssetPointTestCase(unittest.TestCase):
    def test_get_mid(self):
        answer = AssetPoint.get_med(30, 20)
        self.assertEqual(answer, 25)


if __name__ == '__main__':
    unittest.main()
