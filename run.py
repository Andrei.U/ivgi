import app
import json


def read_data_file() -> dict:
    with open('data.json') as file_json:
        return json.load(file_json)


def get_report():
    for asset in app.stream.assets:
        last_point = asset.points[-1:][0]
        if asset.purse.position >= 0:
            last_price = last_point.bid
        else:
            last_price = last_point.ask
        print('Report for {name}, balance after trade = {cost}'.format(name=asset.name,
                                                                       cost=asset.purse.close_position(last_price)))
    print('Trade Strategy: {}'.format(app.strategy.__name__))


if __name__ == '__main__':
    time_stamps = read_data_file()
    for time in time_stamps.keys():
        app.stream.set(time, time_stamps[time])
    get_report()


