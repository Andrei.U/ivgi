import json


class Transaction:
    def __init__(self):
        self.transaction_log = []

    def add(self, time, actions: list):
        print({"time": time, "actions": actions})
        self.transaction_log.append({"time": time, "actions": actions})

    def get_report(self):
        return json.dumps(self.transaction_log)

    def __str__(self):
        return self.get_report()

    def __repr__(self):
        return self.get_report()

