from multiprocessing import Process
from app.strategy import NoRiskStrategy
import app


def statistic_decorator(func):

    def max_func(self, data: float):
        if self.max is None:
            self.max = data
        elif data > self.max:
            self.max = data

    def min_func(self, data: float):
        if self.min is None:
            self.min = data
        elif data < self.min:
            self.min = data

    def dec_args(*args):
        self = args[0]
        data = args[1]
        med = AssetPoint.get_med(data['ask'], data['bid'])
        max_func(self, med)
        min_func(self, med)
        func(args[0], args[1], args[2])

    return dec_args


class AssetPoint:
    def __init__(self, data: dict, timestamp: str = None):
        self.bid = data['bid']
        self.ask = data['ask']
        self.diff = AssetPoint.get_diff(self.ask, self.bid)
        self.med = AssetPoint.get_med(ask=self.ask, bid=self.bid)
        self.timestamp = timestamp

    @staticmethod
    def get_med(ask, bid):
        return (ask+bid)/2

    @staticmethod
    def get_diff(ask, bid):
        return ask-bid


class Asset:
    def __init__(self, name: str):
        self.name = name
        self.points = []
        self.max = None
        self.min = None
        self.purse = Purse()
        self.max_avr_range = app.avr_list_len

    def gate(self):
        return self.max - self.min

    def get_legal_name(self):
        return self.name.strip('asset')

    @statistic_decorator
    def add(self, data: dict, timestamp: str = None):
        self.points.append(AssetPoint(timestamp=timestamp, data=data))

    def get_avr(self) -> float:
        med_sum = 0
        for point in self.points[self.max_avr_range:]:
            med_sum += AssetPoint.get_med(ask=point.ask, bid=point.bid)
        return med_sum/(len(self.points[self.max_avr_range:]))


class Stream:

    def __init__(self):
        self.data_stream = []
        self.output_stream = []
        self.count = 0
        self.assets = []
        self.start_time = None

    def set(self, timestamp: str, assets: dict):
        for key in assets.keys():
            self.__set_asset_data(key, assets[key], timestamp)
        self.__check_time(float(timestamp))

    def __add_to_exist_asset(self, name: str, data: dict, timestamp: str) -> bool:
        for asset in self.assets:
            if name == asset.name:
                asset.add(data, timestamp)
                return True
        return False

    def __set_asset_data(self, name: str, data: dict, timestamp: str):
        if not self.__add_to_exist_asset(name, data, timestamp):
            asset = Asset(name)
            asset.add(data, timestamp)
            self.assets.append(asset)

    def __check_time(self, timestamp: float):
        if not self.start_time or timestamp > self.start_time + 30000:
            self.start_time = timestamp
            output_stream = OutputStream(self.assets)
            output_stream.run()


class Purse:

    def __init__(self):
        self.cash = 0
        self.position = 0

    def buy(self, price):
        self.cash -= price
        self.position += 1

    def cell(self, price):
        self.cash += price
        self.position -= 1

    def close_position(self, price) -> float:
        self.cash = self.cash + (price * self.position)
        return self.cash


class OutputStream:

    def __init__(self, assets: list):
        super(OutputStream, self).__init__()
        self.assets = assets
        self.name = 'OutputStreamProcess'

    def run(self):
        actions = []
        for asset in self.assets:
            action = app.strategy(asset).get_decision()
            if action:
                current_point: AssetPoint = asset.points[-1:][0]
                if action == NoRiskStrategy.actions[0]:
                    asset.purse.cell(current_point.bid)
                    action_name = NoRiskStrategy.actions[0]+asset.get_legal_name()
                    actions.append(action_name)
                elif action == NoRiskStrategy.actions[1]:
                    asset.purse.buy(current_point.ask)
                    action_name = NoRiskStrategy.actions[1]+asset.get_legal_name()
                    actions.append(action_name)
        app.transaction.add(self.assets[0].points[-1:][0].timestamp, actions)
        for asset in self.assets:
            print('Asset Name:{name} cash={cash}, position={position}'.format(name=asset.name,
                                                                              cash=asset.purse.cash,
                                                                              position=asset.purse.position))
            print('points:{}'.format(len(asset.points)))















