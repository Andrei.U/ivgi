
class Strategy:
    actions = ('cell', 'buy')

    def __init__(self, asset):
        self.action = None
        self.asset = asset
        self.current_asset = asset.points[-1:][0]
        self.calc()

    def calc(self):
        pass

    def get_decision(self):
        return self.action


class NoRiskStrategy(Strategy):
    def __init__(self, asset):
        super(NoRiskStrategy, self).__init__(asset)
        self.calc()

    def calc(self):
        avr = self.asset.get_avr()
        if self.current_asset.ask < avr:
            self.action = self.actions[1]
        elif self.current_asset.bid > avr:
            self.action = self.actions[0]
        else:
            self.action = None

